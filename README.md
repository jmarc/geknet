# Geknet #
Geknet is an extension for Kryonet. It provides a layer of features added above the basic TCP/UDP connection which are specifically made for games.

The default implementation will provide an easy plug-n-play API and the default settings are made to work with basic games without effort. There are however settings in the provided configuration for the client and server to provide more fine tuned customization.

The main focus for this project is 2D-games based on Android devices with a client-server architecture. Thus a lot of work will be put into efficiency, bandwidth and removing object allocations so that a mobile device will be able to run as a server host as well. Even though the project has Android as a focus, it has no connections to the Android API and will run on any other platform.

###Download###
More stable releases will be compiled and added to the folder "release".

The released JAR files includes sources.

Since the project is in early development, expect bugs, glitches and crashes.

###Features###
* Entity handling connected to an id in order to keep track of players on both client and server.
* Handles position for all entities.
* Convenient way to add custom objects and handler to receive them.
* Supports both TCP and UDP and easy access to send objects through GekClient/GekServer classes
	* If UDP is not available, TCP will be used instead.

###How to get started###
Examples on how to use the library will be added when the development is stable.

####Server####
Create a class that overwrites GekServer and implement the missing methods, I will call it NewServer.

1. In the main class, create a new instance of NewServer.
2. To start the server, call newServer.connect();
3. Add any custom object you need through newServer.registerObject(CustomObject);
3. During lifetime of the application, call newServer.update(delta);
4. To stop the server, call newServer.disconnect();

**delta** is a float set to time since last call in seconds.

**Note** that you need to register the same objects on both the client and the server in the **same order**.

####Client####
Create a class that overwrites GekClient and implement the missing methods, I will call it NewClient.

1. In the main class, create a new instance of NewClient.
2. To start the server, call newClient.connect();
3. At start of the application:
    1. Add any custom objects you need through newClient.registerObject(CustomObject);
    2. Call newClient.setHandlers(ClientHandler, GameHandler, bool); to supply the needed ways of interaction with your game.
4. During lifetime of the application, call newClient.update(delta);
5. To stop the server, call newClient.disconnect();

**delta** is a float set to time since last call in seconds.

**Note** that you need to register the same objects on both the client and the server in the **same order**.

###FAQ###

####Movement is not smooth, it's lagging!####
The library delivers the last known position by the server to the clients. Transfering data over the network introduces delays which will make the movement stutter and uneven. The client has to interpolate the position when rendering.

There is a great guide on how to deal with it here: http://www.gabrielgambetta.com/fast_paced_multiplayer.html

An example will be provided when development is stable.

####How do I send actions instead of position####
There are two steps that needs to be taken in order to replace the position sending from client.

1. On the client: Overwrite the broadcast method and send actions instead, **remember** to add them as custom objects.
2. On the server: When adding the custom objects, from the handlers that are defined call serverHandler.updatePosition(id, x, y).

###License###
This project is licensed by [MIT License](http://opensource.org/licenses/MIT).
Basically, you can do whatever you want as long as you include the original copyright and license notice in any copy of the source.

###Kryonet###
This project is based on the awesome networking library Kryonet made by Nathan Sweet. I suggest you check it out in the repository and learn how it works if you want to understand what goes on behind the scenes.
[Link to repository](https://github.com/EsotericSoftware/kryonet)
License for Kryonet included in the file kryonetlicense.txt