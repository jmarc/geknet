package se.jmarc.geknet.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import se.jmarc.geknet.utils.Entity;
import se.jmarc.geknet.utils.EntityArray;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class EntityArrayTest {

    private EntityArray array;

    @Before
    public void setUp() throws Exception {
        array = new EntityArray(5);
    }

    @After
    public void tearDown() throws Exception {
        array = null;
    }

    @Test
    public void testGet() throws Exception {
        for (int i = 0; i < array.size; i++) {
            assertNotEquals(null, array.get(i));
        }
    }

    @Test
    public void testAdd() throws Exception {
        array.add(getEntityWithId(1));
        array.add(getEntityWithId(2));

        assertEquals(1, array.get(0).id);
        assertEquals(2, array.get(1).id);
        assertEquals(null, array.get(2));
    }

    @Test
    public void testRemove() throws Exception {
        array.add(getEntityWithId(1));
        array.add(getEntityWithId(2));
        array.add(getEntityWithId(3));
        array.add(getEntityWithId(4));

        array.remove(1);

        assertEquals(1, array.get(0).id);
        assertEquals(4, array.get(1).id);
        assertEquals(3, array.get(2).id);
        assertEquals(null, array.get(3));
    }

    public Entity getEntityWithId(int id) {
        Entity entity = new Entity();
        entity.id = id;
        return entity;
    }
}