package se.jmarc.geknet.tests.implementations;

import se.jmarc.geknet.common.Configuration;
import se.jmarc.geknet.server.GekServer;
import se.jmarc.geknet.utils.Vector2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marc
 * @version 1.0
 */
public class ServerConnection extends GekServer {

    private Map<Integer, Vector2> positions = new HashMap<Integer, Vector2>();

    public ServerConnection(Configuration configuration) throws IOException {
        super(configuration);
    }

    public void clientConnected(int id) {
        positions.put(id, new Vector2());
    }

    public void clientDisconnected(int id) {
        positions.remove(id);
    }

    public void getPosition(int id, Vector2 position) {
        Vector2 p = positions.get(id);
        position.x = p.x;
        position.y = p.y;
    }

    public void newPosition(int id, float x, float y) {
        Vector2 position = positions.get(id);
        position.x = x;
        position.y = y;
    }
}
