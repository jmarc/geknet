package se.jmarc.geknet.tests.implementations;

import se.jmarc.geknet.client.ClientHandler;
import se.jmarc.geknet.client.GekClient;
import se.jmarc.geknet.common.Configuration;
import se.jmarc.geknet.utils.Vector2;

/**
 * @author Marc
 * @version 1.0
 */
public class ClientConnection extends GekClient<Void> implements ClientHandler {

    public final Vector2 position = new Vector2();

    public ClientConnection(Configuration configuration) {
        super(configuration);
    }

    public void onConnect(int id) {
        System.out.println(Thread.currentThread().getName() + " onConnect");
    }

    public void onDisconnect() {
        System.out.println(Thread.currentThread().getName() + " onDisconnect");
    }

    public void clientList(int[] ids) {
        System.out.println(Thread.currentThread().getName() + " clientList");
    }

    public void clientConnected(int id) {
        System.out.println(Thread.currentThread().getName() + " clientConnected");
    }

    public void clientDisconnected(int id) {
        System.out.println(Thread.currentThread().getName() + " clientDisconnected");
    }

    public void playerPosition(Vector2 position) {
        System.out.println(Thread.currentThread().getName() + " playerPosition");

        position.x = this.position.x;
        position.y = this.position.y;
    }

    public void clientPosition(int id, float x, float y) {
        System.out.println(Thread.currentThread().getName() + " clientPosition");
    }

    public void objectUnknown(Object object) {

    }
}
