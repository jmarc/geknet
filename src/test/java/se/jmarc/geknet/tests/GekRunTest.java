package se.jmarc.geknet.tests;

import com.esotericsoftware.minlog.Log;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import se.jmarc.geknet.client.ClientConnectionHandler;
import se.jmarc.geknet.common.Configuration;
import se.jmarc.geknet.common.logging.FileLog;
import se.jmarc.geknet.server.ServerHandler;
import se.jmarc.geknet.tests.implementations.ClientConnection;
import se.jmarc.geknet.tests.implementations.ServerConnection;
import se.jmarc.geknet.tests.network.Greeting;
import se.jmarc.geknet.utils.CustomObject;

import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GekRunTest {

    private static ServerConnection serverConnection;
    private static ClientConnection clientConnection1;
    private static ClientConnection clientConnection2;

    @BeforeClass
    public static void setUp() throws Exception {
        Log.set(Log.LEVEL_ERROR);
        Thread.currentThread().setName("Game");

        Configuration serverConfiguration = new Configuration("localhost", 3000, 5);
        serverConfiguration.broadcastDelay = 1;
        serverConfiguration.log = new FileLog(false, "Server.txt");
        serverConnection = new ServerConnection(serverConfiguration);
        serverConnection.connect();
        serverConnection.registerObject(new CustomObject<Integer, ServerHandler, Greeting>(Greeting.class) {
            @Override
            public void receive(Integer integer, ServerHandler serverHandler, Object object) {
                Greeting greeting = (Greeting) object;
                serverHandler.info("Greeting", greeting.message);
            }
        });

        Configuration clientConfiguration1 = new Configuration("localhost", 3000, 5);
        clientConfiguration1.broadcastDelay = 1;
        clientConfiguration1.log = new FileLog(false, "Client 1.txt");
        clientConnection1 = new ClientConnection(clientConfiguration1);
        clientConnection1.setHandlers(clientConnection1, null, true);
        clientConnection1.connect();

        // Connect second client after 2 seconds
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Configuration clientConfiguration2 = new Configuration("localhost", 3000, 5);
                clientConfiguration2.broadcastDelay = 1;
                clientConfiguration2.log = new FileLog(false, "Client 2.txt");
                clientConnection2 = new ClientConnection(clientConfiguration2);
                try {
                    clientConnection2.connect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 2000);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                clientConnection2.setHandlers(clientConnection2, null, true);
                clientConnection2.registerObject(new CustomObject<ClientConnectionHandler, Void, Greeting>(Greeting.class) {
                    @Override
                    public void receive(ClientConnectionHandler connectionHandler, Void aVoid, Object object) {
                        Greeting greeting = (Greeting) object;
                        connectionHandler.info("Greeting", greeting.message);
                    }
                });

                serverConnection.sendTCP(2, new Greeting("Off to sleepy land"));
            }
        }, 3000);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        Thread.sleep(1000);
        serverConnection.disconnect();

        serverConnection = null;
        clientConnection1 = null;
        clientConnection2 = null;
    }

    @Test
    public void testUpdatePosition() throws Exception {
        long totalTime = 0;
        long time = System.currentTimeMillis();
        while (totalTime < 6000) {
            long newTime = System.currentTimeMillis();
            float delta = newTime - time;
            totalTime += delta;
            time = newTime;
            delta /= 1000.0f;

            clientConnection1.position.x = new Random().nextFloat() * 20 - 10;
            serverConnection.update(delta);
            clientConnection1.update(delta);
            if (clientConnection2 != null)
                clientConnection2.update(delta);
        }
    }
}