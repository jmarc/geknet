package se.jmarc.geknet.tests.network;

/**
 * @author Marc
 * @version 1.0
 */
public class Greeting {
    public String message;

    public Greeting() {
    }

    public Greeting(String message) {
        this.message = message;
    }
}
