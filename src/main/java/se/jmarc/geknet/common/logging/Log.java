package se.jmarc.geknet.common.logging;

/**
 * Handles logging of the client and server<br>
 * Severity: info &lt; debug &lt; error
 *
 * @author Marc
 */
public interface Log {

    /**
     * Logs a debug message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    void debug(String tag, String message);

    /**
     * Logs an error message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    void error(String tag, String message);

    /**
     * Logs an info message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    void info(String tag, String message);
}
