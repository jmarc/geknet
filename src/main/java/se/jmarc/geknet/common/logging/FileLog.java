package se.jmarc.geknet.common.logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Handles logging of the client and server to a file<br>
 * Severity: info &lt; debug &lt; error
 *
 * @author Marc
 */
public class FileLog implements Log {

    /** Used for getting the current time */
    private final Calendar calendar;

    /** Writes to the text file */
    private BufferedWriter writer;

    /**
     * Creates a log that is written to a text file
     *
     * @param uniqueLog If each run should end up in a new text file, appended by date and time
     * @param title     Title for the log file
     */
    public FileLog(boolean uniqueLog, String title) {
        calendar = Calendar.getInstance();

        if (title == null) {
            title = "";
        } else if (uniqueLog) {
            title = " " + title;
        }

        if (uniqueLog) {
            title = getTimeString("yyyy-MM-dd HH.mm.ss") + title;
        }

        File logFile = new File(title);
        try {
            writer = new BufferedWriter(new FileWriter(logFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Convenient method for formatting the time string
     *
     * @param format Format of the time string
     * @return The formatted time string
     */
    private String getTimeString(String format) {
        return new SimpleDateFormat(format).format(calendar.getTime());
    }

    /**
     * Writes the given data to the log file
     *
     * @param type    Severity
     * @param tag     Tag of message
     * @param message Message
     */
    private void writeToFile(String type, String tag, String message) {
        try {
            String time = getTimeString("MMdd HH:mm");
            writer.write(String.format("[%s] %s - %s: %s", time, type, tag, message));
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void debug(String tag, String message) {
        writeToFile("debug", tag, message);
    }

    public synchronized void error(String tag, String message) {
        writeToFile("error", tag, message);
    }

    public synchronized void info(String tag, String message) {
        writeToFile("info", tag, message);
    }
}
