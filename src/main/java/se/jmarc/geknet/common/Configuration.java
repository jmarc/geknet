package se.jmarc.geknet.common;

import se.jmarc.geknet.common.logging.Log;

/**
 * Settings used for the client and server. It is important that both gets the same max amount of players set.
 *
 * @author Marc
 * @version 1.0
 */
public class Configuration {

    // Required settings

    /**
     * Server host address
     */
    public final String host;
    /**
     * Server TCP port
     */
    public final int tcpPort;
    /**
     * Server UDP port, null if not used
     */
    public final Integer udpPort;
    /**
     * Max number of players on the server
     */
    public final int maxPlayers;


    // Optional settings

    /**
     * How logging should be made, null for none
     */
    public Log log = null;
    /**
     * Time between pulling received messages in seconds. Default is 10 times per second
     */
    public float runnableDelay = 1.0f / 10.0f;
    /**
     * Time between broadcasting position. Default is 10 times per second
     */
    public float broadcastDelay = 1.0f / 10.0f;


    /**
     * Initializes only with TCP, without UDP
     *
     * @param host       Server host address
     * @param tcpPort    TCP port
     * @param maxPlayers Max amount of players on the server
     */
    public Configuration(String host, int tcpPort, int maxPlayers) {
        this.host = host;
        this.tcpPort = tcpPort;
        this.udpPort = null;
        this.maxPlayers = maxPlayers;
    }

    /**
     * Initializes with both TCP and UDP
     *
     * @param host       Server host address
     * @param tcpPort    TCP port
     * @param udpPort    UDP port
     * @param maxPlayers Max amount of players on the server
     */
    public Configuration(String host, int tcpPort, int udpPort, int maxPlayers) {
        this.host = host;
        this.tcpPort = tcpPort;
        this.udpPort = udpPort;
        this.maxPlayers = maxPlayers;
    }

}
