package se.jmarc.geknet.common.serializers;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import se.jmarc.geknet.utils.Entity;
import se.jmarc.geknet.utils.EntityArray;

/**
 * Serializer for entities to bypass object allocations
 *
 * @author Marc Jamot
 */
public class EntityArraySerializer extends Serializer<EntityArray> {

    /** Pre-allocated entity array of the size of max allowed players */
    private final EntityArray tmpArray;

    {
        setAcceptsNull(true);
        setImmutable(true);
    }

    /**
     * Pre-allocates the entity array
     *
     * @param maxPlayers Number of players allowed on the server
     */
    public EntityArraySerializer(int maxPlayers) {
        tmpArray = new EntityArray(maxPlayers);

        for (int i = 0; i < tmpArray.capacity; i++) {
            tmpArray.add(new Entity());
        }
    }

    @Override
    public void write(Kryo kryo, Output output, EntityArray array) {
        int size = array.size;

        output.writeInt(size);
        for (int i = 0; i < size; i++) {
            Entity entity = array.get(i);
            output.writeInt(entity.id);
            output.writeFloat(entity.position.x);
            output.writeFloat(entity.position.y);
        }
    }

    @Override
    public EntityArray read(Kryo kryo, Input input, Class<EntityArray> type) {
        tmpArray.size = input.readInt();

        for (int i = 0; i < tmpArray.size; i++) {
            Entity entity = tmpArray.get(i);
            entity.id = input.readInt();
            entity.position.x = input.readFloat();
            entity.position.y = input.readFloat();
        }

        return tmpArray;
    }
}
