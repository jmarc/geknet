package se.jmarc.geknet.common.objects;

/**
 * Another client has connected to the server
 *
 * @author Marc Jamot
 */
public class ClientConnected {

    /** Id of the connected client */
    public int id;

    /**
     * Serialization needs empty constructor
     */
    public ClientConnected() {
    }

    /**
     * Sets the id
     *
     * @param id Id of the connected client
     */
    public ClientConnected(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ClientConnected[" + id + "]";
    }
}
