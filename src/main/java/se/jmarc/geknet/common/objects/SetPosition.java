package se.jmarc.geknet.common.objects;

/**
 * Tells the client to use the given position
 *
 * @author Marc Jamot
 */
public class SetPosition {

    /** X coordinate */
    public float x;

    /** Y coordinate */
    public float y;

    /**
     * Serialization needs empty constructor
     */
    public SetPosition() {
    }

    /**
     * Sets the position
     *
     * @param x New X coordinate
     * @param y New Y coordinate
     */
    public SetPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("Set(%s, %s)", x, y);
    }
}
