package se.jmarc.geknet.common.objects;

/**
 * Another client has disconnected from the server
 *
 * @author Marc Jamot
 */
public class ClientDisconnected {

    /** Id of the connected client */
    public int id;

    /**
     * Serialization needs empty constructor
     */
    public ClientDisconnected() {
    }

    /**
     * Sets the id
     *
     * @param id Id of the disconnected client
     */
    public ClientDisconnected(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ClientDisconnected[" + id + "]";
    }
}
