package se.jmarc.geknet.common.objects;

import java.util.Arrays;

/**
 * List of active clients on the server
 *
 * @author Marc Jamot
 */
public class ClientList {

    /** List of active client ids */
    public int[] ids;

    /**
     * Serialization needs empty constructor
     */
    public ClientList() {
    }

    /**
     * Sets the list of client ids
     *
     * @param ids List of active client ids
     */
    public ClientList(int[] ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "ClientList " + Arrays.toString(ids);
    }
}
