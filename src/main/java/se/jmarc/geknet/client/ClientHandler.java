package se.jmarc.geknet.client;

import se.jmarc.geknet.utils.Vector2;

/**
 * Interface passed from the client to the listener, handles response from the server
 *
 * @author Marc Jamot
 */
public interface ClientHandler {

    /**
     * Called when successfully connected to the server
     *
     * @param id Client id
     */
    void onConnect(int id);

    /** Called when disconnected from the server */
    void onDisconnect();

    /**
     * Called when a client list was received from the server, should match the list of active players
     *
     * @param ids Client ids
     */
    void clientList(int[] ids);

    /**
     * Called when another client connects to the server
     *
     * @param id Client id
     */
    void clientConnected(int id);

    /**
     * Called when another client disconnects from the server
     *
     * @param id Client id
     */
    void clientDisconnected(int id);

    /**
     * Update the values of the provided Vector2 with the position of the entity that belongs to the player. This is
     * used in order to prevent object allocation.<br>
     * position.x = ...;<br>
     * position.y = ...;
     *
     * @param position Object used to get the position and velocity of the entity
     */
    void playerPosition(Vector2 position);

    /**
     * New position received for a client
     *
     * @param id Id of client to get position for
     * @param x  X coordinate of client
     * @param y  Y coordinate of client
     */
    void clientPosition(int id, float x, float y);
}
