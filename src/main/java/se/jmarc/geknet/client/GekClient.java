package se.jmarc.geknet.client;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import se.jmarc.geknet.common.Configuration;
import se.jmarc.geknet.utils.CustomObject;
import se.jmarc.geknet.utils.GekBase;
import se.jmarc.geknet.utils.Vector2;

import java.io.IOException;

/**
 * Main class for the client
 *
 * @author Marc Jamot
 * @since 2015-06
 */
public class GekClient<GameHandler> extends GekBase implements ClientConnectionHandler {

    /** Client listener that receives everything from the server */
    private final ClientListener<GameHandler> clientListener;
    /** Temporary Vector2 to keep track of player position and velocity */
    private final Vector2 position = new Vector2();
    /** Kryonet client */
    private Client kryoClient;
    /** Handler for responding to server messages */
    private ClientHandler clientHandler;

    /**
     * Initializes the client<br>
     * Lifecycle: connect() -&gt; update(float) -&gt; disconnect()<br>
     * Remember to keep calling update(float) in the main loop until the client disconnects
     *
     * @param configuration Configuration to be used by the client.<br><b>IMPORTANT</b>: must be the same as for the server!
     */
    public GekClient(Configuration configuration) {
        super(configuration);

        clientListener = new ClientListener<GameHandler>(this, queue, configuration);
        kryoClient.addListener(clientListener);
    }

    @Override
    public Kryo initialize() {
        kryoClient = new Client();
        return kryoClient.getKryo();
    }

    @Override
    public void connect() throws IOException {
        this.kryoClient.start();
        if (udpPort == null) {
            this.kryoClient.connect(5000, host, tcpPort);
        } else {
            this.kryoClient.connect(5000, host, tcpPort, udpPort);
        }
    }

    @Override
    public void disconnect() {
        kryoClient.stop();
    }

    @Override
    public void onBroadcast() {
        if (clientHandler != null) {
            clientHandler.playerPosition(position);
            sendUDP(position);
        }
    }

    /**
     * Send an object over the network using TCP
     *
     * @param object Object to send
     */
    public void sendTCP(Object object) {
        kryoClient.sendTCP(object);
    }

    /**
     * Send an object over the network using UDP
     *
     * @param object Object to send
     */
    public void sendUDP(Object object) {
        if (UDPConnection) kryoClient.sendUDP(object);
        else sendTCP(object);
    }

    /**
     * Sets the handler to respond to server messages
     *
     * @param clientHandler       Handler for responding to server messages
     * @param handler             Handler to connect to game situations
     * @param parseMissedMessages If buffered messages should be handled
     */
    public void setHandlers(ClientHandler clientHandler, GameHandler handler, boolean parseMissedMessages) {
        this.clientHandler = clientHandler;
        clientListener.setHandlers(clientHandler, handler, parseMissedMessages);
    }

    /**
     * Registers an object to be used with the client and server
     *
     * @param customObject Object to register
     */
    public void registerObject(CustomObject<ClientConnectionHandler, GameHandler, ?> customObject) {
        registerKryo(customObject);
        clientListener.register(customObject);
    }

    /**
     * Logs a debug message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    public void debug(String tag, String message) {
        if (log != null) log.debug(tag, message);
    }

    /**
     * Logs an error message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    public void error(String tag, String message) {
        if (log != null) log.error(tag, message);
    }

    /**
     * Logs an info message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    public void info(String tag, String message) {
        if (log != null) log.info(tag, message);
    }
}
