package se.jmarc.geknet.client;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import se.jmarc.geknet.common.Configuration;
import se.jmarc.geknet.common.logging.Log;
import se.jmarc.geknet.common.objects.*;
import se.jmarc.geknet.common.objects.Shutdown;
import se.jmarc.geknet.utils.CustomObject;
import se.jmarc.geknet.utils.Entity;
import se.jmarc.geknet.utils.EntityArray;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Handles messages sent from the server
 *
 * @param <GameHandler> Handler for accessing the game when a custom object is received
 * @author Marc Jamot
 */
public class ClientListener<GameHandler> extends Listener {

    private final BlockingQueue<RunHolder> runOnClientQueue = new LinkedBlockingQueue<RunHolder>();
    /** Handler for sending network data when a custom object is received */
    private final ClientConnectionHandler connectionHandler;
    /** Queue of Runnable that is run on the main thread when polled at a later time */
    private final BlockingQueue<Runnable> queue;
    /** How logging should be made, null for none */
    private final Log log;
    /** Custom objects */
    private final LinkedList<CustomObject<ClientConnectionHandler, GameHandler, ?>> customObjects = new LinkedList<CustomObject<ClientConnectionHandler, GameHandler, ?>>();
    /** Handler for responding to server messages */
    private ClientHandler clientHandler;
    /** If the client has received a client list from the server */
    private volatile boolean initialized = false;
    /** Handler for accessing the game when a custom object is received */
    private GameHandler gameHandler;

    /**
     * Sets the required references
     *
     * @param connectionHandler Reference to the handler for the GekClient
     * @param queue             Queue of Runnable that is run on the main thread when polled at a later time
     * @param configuration     Client configuration
     */
    public ClientListener(ClientConnectionHandler connectionHandler, BlockingQueue<Runnable> queue, Configuration configuration) {
        this.connectionHandler = connectionHandler;
        this.queue = queue;
        this.log = configuration.log;
    }

    @Override
    public void connected(Connection connection) {
        int id = connection.getID();
        runOnClient(RunType.CONNECTED, id, null);
    }

    @Override
    public void disconnected(Connection connection) {
        int id = connection.getID();
        runOnClient(RunType.DISCONNECTED, id, null);
    }

    @Override
    public void received(Connection connection, Object object) {
        int id = connection.getID();
        runOnClient(RunType.RECEIVED, id, object);
    }

    /**
     * Sets the handler to respond to server messages
     *
     * @param clientHandler       Handler for responding to server messages
     * @param gameHandler         Handler for accessing the game when a custom object is received
     * @param parseMissedMessages If buffered messages should be handled
     */
    public void setHandlers(ClientHandler clientHandler, GameHandler gameHandler, boolean parseMissedMessages) {
        this.clientHandler = clientHandler;
        this.gameHandler = gameHandler;

        if (log != null) log.debug("setClientHandler", "Added client handler");
        if (parseMissedMessages) {
            for (RunHolder runHolder = runOnClientQueue.poll(); runHolder != null; runHolder = runOnClientQueue.poll()) {
                runOnClient(runHolder.type, runHolder.id, runHolder.object);
            }
        } else {
            runOnClientQueue.clear();
        }
    }

    /**
     * Registers a custom object to be sent between the client and server
     *
     * @param customObject Custom object
     */
    public void register(CustomObject<ClientConnectionHandler, GameHandler, ?> customObject) {
        customObjects.add(customObject);
    }

    /**
     * Calls the client handler or stores the call if no handler is assigned
     *
     * @param type   Call type
     * @param id     Connection id
     * @param object Received object
     */
    private void runOnClient(RunType type, int id, Object object) {
        if (clientHandler == null) {
            runOnClientQueue.add(new RunHolder(type, id, object));
        } else {
            switch (type) {
                case CONNECTED:
                    connected(id);
                    break;

                case DISCONNECTED:
                    disconnected(id);
                    break;

                case RECEIVED:
                    received(id, object);
                    break;
            }
        }
    }

    /**
     * Calls onConnect on main thread
     *
     * @param id Client id
     */
    private void connected(final int id) {
        if (log != null) log.info("connected", "Id: " + id);
        queue.add(new Runnable() {
            public void run() {
                clientHandler.onConnect(id);
            }
        });
    }

    /**
     * Calls onDisconnect on main thread
     *
     * @param id Client id
     */
    private void disconnected(int id) {
        if (log != null) log.info("disconnected", "Id: " + id);
        queue.add(new Runnable() {
            public void run() {
                clientHandler.onDisconnect();
            }
        });
    }

    /**
     * Calls onReceived on main thread
     *
     * @param connectionId Client id
     * @param object       Received object
     */
    private void received(final int connectionId, Object object) {
        // Kryo messages

        //noinspection StatementWithEmptyBody
        if (object instanceof FrameworkMessage.KeepAlive) {
        }

        // Connection messages

        // When a client connects
        else if (object instanceof ClientConnected) {
            if (log != null) log.info("received", "Client connected with id " + ((ClientConnected) object).id);
            final int id = ((ClientConnected) object).id;
            queue.add(new Runnable() {
                public void run() {
                    clientHandler.clientConnected(id);
                }
            });
        }

        // When a client disconnects
        else if (object instanceof ClientDisconnected) {
            if (log != null) log.info("received", "Client disconnected with id " + ((ClientDisconnected) object).id);
            final int id = ((ClientDisconnected) object).id;
            queue.add(new Runnable() {
                public void run() {
                    clientHandler.clientDisconnected(id);
                }
            });
        }

        // When the server is full
        else if (object instanceof ServerFull) {
            if (log != null) log.info("received", "Server is full");
            queue.add(new Runnable() {
                public void run() {
                    connectionHandler.disconnect();
                }
            });
        }

        // When the server is shut down
        else if (object instanceof Shutdown) {
            if (log != null) log.info("received", "Shut down");
            queue.add(new Runnable() {
                public void run() {
                    connectionHandler.disconnect();
                }
            });
        }

        // When a list of clients are received
        else if (object instanceof ClientList) {
            final int[] ids = ((ClientList) object).ids;
            if (log != null) log.info("received", "Client list: " + Arrays.toString(ids));
            queue.add(new Runnable() {
                public void run() {
                    clientHandler.clientList(ids);
                    initialized = true;
                }
            });
        }

        if (initialized) {
            // Entity messages
            if (object instanceof EntityArray) {
                EntityArray array = (EntityArray) object;

                for (int i = 0; i < array.size; i++) {
                    Entity entity = array.get(i);
                    final int id = entity.id;

                    if (id != connectionId) {
                        final float x = entity.position.x;
                        final float y = entity.position.y;
                        queue.add(new Runnable() {
                            public void run() {
                                clientHandler.clientPosition(id, x, y);
                            }
                        });
                    }
                }
            }

            // Custom objects
            else {
                boolean noMatchFound = true;

                for (final CustomObject<ClientConnectionHandler, GameHandler, ?> customObject : customObjects) {
                    if (object.getClass() == customObject.tClass) {
                        noMatchFound = false;
                        final Object o = object;
                        queue.add(new Runnable() {
                            public void run() {
                                customObject.receive(connectionHandler, gameHandler, o);
                            }
                        });
                    }
                }

                // This should never be called in theory. It will only happen if the Kryo object is directly changed
                if (noMatchFound) {
                    if (log != null) log.debug("received[" + connectionId + "]", object.toString());
                }
            }
        }
    }

    /** Type of runnable that can be performed */
    enum RunType {
        CONNECTED, DISCONNECTED, RECEIVED
    }

    /**
     * Class for storing a client call to be done when a handler is assigned
     */
    class RunHolder {
        public final RunType type;
        public final int id;
        public final Object object;

        public RunHolder(RunType type, int id, Object object) {
            this.type = type;
            this.id = id;
            this.object = object;
        }
    }
}
