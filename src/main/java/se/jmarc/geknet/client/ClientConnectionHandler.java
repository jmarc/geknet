package se.jmarc.geknet.client;

import se.jmarc.geknet.common.logging.Log;

/**
 * @author Marc
 * @version 1.0
 */
public interface ClientConnectionHandler extends Log {

    /** Close the connection */
    void disconnect();

    /**
     * Send an object through TCP
     *
     * @param object The object to send
     */
    void sendTCP(Object object);

    /**
     * Send an object through UDP
     *
     * @param object The object to send
     */
    void sendUDP(Object object);

}
