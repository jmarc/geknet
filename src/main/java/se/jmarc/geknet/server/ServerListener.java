package se.jmarc.geknet.server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import se.jmarc.geknet.common.Configuration;
import se.jmarc.geknet.common.logging.Log;
import se.jmarc.geknet.common.objects.ClientConnected;
import se.jmarc.geknet.common.objects.ClientDisconnected;
import se.jmarc.geknet.common.objects.ClientList;
import se.jmarc.geknet.common.objects.ServerFull;
import se.jmarc.geknet.utils.CustomObject;
import se.jmarc.geknet.utils.Vector2;

import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

/**
 * Handles messages sent from the clients
 *
 * @author Marc Jamot
 */
public class ServerListener extends Listener {

    /** Reference to send TCP response to the client */
    private final ServerHandler server;
    /** Queue of Runnable that is run on the main thread when polled at a later time */
    private final BlockingQueue<Runnable> queue;
    /** How logging should be made, null for none */
    private final Log log;
    /** Max number of allowed players on the server */
    private final int maxPlayers;
    /** Custom objects */
    private final LinkedList<CustomObject<Integer, ServerHandler, ?>> customObjects = new LinkedList<CustomObject<Integer, ServerHandler, ?>>();
    /** Holds count of how many clients that are connected */
    private int connected;

    /**
     * Sets the required references
     *
     * @param server        Reference to the handler for the GekServer
     * @param queue         Queue of Runnable that is run on the main thread when polled at a later time
     * @param configuration Server configuration
     */
    public ServerListener(ServerHandler server, BlockingQueue<Runnable> queue, Configuration configuration) {
        this.server = server;
        this.queue = queue;
        this.log = configuration.log;
        this.maxPlayers = configuration.maxPlayers;
    }

    @Override
    public void connected(Connection connection) {
        if (log != null) log.info("connected", connection.toString());
        connected += 1;
        final int id = connection.getID();

        if (connected > maxPlayers) {
            server.sendTCP(id, new ServerFull());
        } else {
            server.sendTCP(id, new ClientList(server.getClientIds()));
            server.sendTCPExcept(id, new ClientConnected(id));

            queue.add(new Runnable() {
                public void run() {
                    server.addEntity(id);
                    server.clientConnected(id);
                }
            });
        }
    }

    @Override
    public void disconnected(Connection connection) {
        if (log != null) log.info("disconnected", connection.toString());
        connected -= 1;

        final int id = connection.getID();
        if (server.hasEntity(id)) {
            server.sendTCPAll(new ClientDisconnected(id));

            queue.add(new Runnable() {
                public void run() {
                    server.removeEntity(id);
                    server.clientDisconnected(id);
                }
            });
        }
    }

    @Override
    public void received(Connection connection, Object object) {
        // Kryo messages

        //noinspection StatementWithEmptyBody
        if (object instanceof FrameworkMessage.KeepAlive) {
        }

        // Entity messages
        else if (object instanceof Vector2) {
            Vector2 position = (Vector2) object;
            final int id = connection.getID();
            final float x = position.x;
            final float y = position.y;
            queue.add(new Runnable() {
                public void run() {
                    server.updatePosition(id, x, y);
                }
            });
        }

        // Custom objects
        else {
            boolean noMatchFound = true;

            for (final CustomObject<Integer, ServerHandler, ?> customObject : customObjects) {
                if (object.getClass() == customObject.tClass) {
                    noMatchFound = false;
                    final int id = connection.getID();
                    final Object o = object;
                    queue.add(new Runnable() {
                        public void run() {
                            customObject.receive(id, server, o);
                        }
                    });
                }
            }

            // This should never be called in theory. It will only happen if the Kryo object is directly changed
            if (noMatchFound) {
                if (log != null) log.debug("received[" + connection.getID() + "]", object.toString());
            }
        }
    }

    /**
     * Registers a custom object to be sent between the client and server
     *
     * @param customObject Custom object
     */
    public void register(CustomObject<Integer, ServerHandler, ?> customObject) {
        customObjects.add(customObject);
    }
}
