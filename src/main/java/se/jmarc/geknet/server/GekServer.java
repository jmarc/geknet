package se.jmarc.geknet.server;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;
import se.jmarc.geknet.common.Configuration;
import se.jmarc.geknet.common.objects.Shutdown;
import se.jmarc.geknet.utils.CustomObject;
import se.jmarc.geknet.utils.Entity;
import se.jmarc.geknet.utils.EntityArray;
import se.jmarc.geknet.utils.GekBase;

import java.io.IOException;

/**
 * Main class for the server
 *
 * @author Marc Jamot
 */
public abstract class GekServer extends GekBase implements ServerHandler {

    /** Server listener that receives everything from the clients */
    private final ServerListener serverListener;
    /** Array of clients */
    private final EntityArray entities;
    /** Reference to the Kryonet server */
    private Server kryoServer;

    /**
     * Initializes the server<br>
     * Lifecycle: connect() -&gt; update(float) -&gt; shutdown()<br>
     * Remember to keep calling update(float) in the main loop until the server shut down
     *
     * @param configuration Configuration to be used by the server.<br><b>IMPORTANT</b>: must be the same as for the clients!
     */
    public GekServer(Configuration configuration) {
        super(configuration);
        int maxPlayers = configuration.maxPlayers;

        entities = new EntityArray(maxPlayers);
        serverListener = new ServerListener(this, queue, configuration);
        kryoServer.addListener(serverListener);
    }

    @Override
    public Kryo initialize() {
        kryoServer = new Server();
        return kryoServer.getKryo();
    }

    @Override
    public void connect() throws IOException {
        kryoServer.start();
        if (udpPort == null) {
            kryoServer.bind(tcpPort);
        } else {
            kryoServer.bind(tcpPort, udpPort);
        }
    }

    @Override
    public void onBroadcast() {
        for (int i = 0; i < entities.size; i++) {
            Entity entity = entities.get(i);
            getPosition(entity.id, entity.position);
        }
        sendUDPAll(entities);
    }

    @Override
    public void disconnect() {
        kryoServer.sendToAllTCP(new Shutdown());
        kryoServer.stop();
    }

    /**
     * Logs a debug message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    public void debug(String tag, String message) {
        if (log != null) log.debug(tag, message);
    }

    /**
     * Logs an error message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    public void error(String tag, String message) {
        if (log != null) log.error(tag, message);
    }

    /**
     * Logs an info message
     *
     * @param tag     Tag for the message
     * @param message Message
     */
    public void info(String tag, String message) {
        if (log != null) log.info(tag, message);
    }

    /**
     * Registers an object to be used with the client and server
     *
     * @param customObject Object to register
     */
    public void registerObject(CustomObject<Integer, ServerHandler, ?> customObject) {
        registerKryo(customObject);
        serverListener.register(customObject);
    }

    /**
     * Creates a list of connected ids
     *
     * @return List of connected client ids
     */
    public int[] getClientIds() {
        Connection[] connections = kryoServer.getConnections();
        int[] ids = new int[connections.length];
        for (int i = 0; i < connections.length; i++) {
            ids[i] = connections[i].getID();
        }
        return ids;
    }

    /**
     * Send an object over the network using TCP to a client
     *
     * @param id     Client id
     * @param object Object to send
     */
    public void sendTCP(int id, Object object) {
        kryoServer.sendToTCP(id, object);
    }

    /**
     * Send an object over the network using TCP to all client
     *
     * @param object Object to send
     */
    public void sendTCPAll(Object object) {
        kryoServer.sendToAllTCP(object);
    }

    /**
     * Send an object over the network using TCP to all except one client
     *
     * @param id     Client id
     * @param object Object to send
     */
    public void sendTCPExcept(int id, Object object) {
        kryoServer.sendToAllExceptTCP(id, object);
    }

    /**
     * Send an object over the network using UDP to a client
     *
     * @param id     Client id
     * @param object Object to send
     */
    public void sendUDP(int id, Object object) {
        if (UDPConnection) kryoServer.sendToUDP(id, object);
        else sendTCP(id, object);
    }

    /**
     * Send an object over the network using UDP to all client
     *
     * @param object Object to send
     */
    public void sendUDPAll(Object object) {
        if (UDPConnection) kryoServer.sendToAllUDP(object);
        else sendTCPAll(object);
    }

    /**
     * Send an object over the network using UDP to all except one client
     *
     * @param id     Client id
     * @param object Object to send
     */
    public void sendUDPExcept(int id, Object object) {
        if (UDPConnection) kryoServer.sendToAllExceptUDP(id, object);
        else sendTCPExcept(id, object);
    }

    /**
     * Add a client to the server
     *
     * @param id Client id
     */
    public void addEntity(int id) {
        Entity entity = new Entity();
        entity.id = id;
        entities.add(entity);
    }

    /**
     * Checks if client exists on the server
     *
     * @param id Client id
     * @return If client exists on the server
     */
    public boolean hasEntity(int id) {
        for (int i = 0; i < entities.size; i++) {
            if (entities.get(i).id == id)
                return true;
        }
        return false;
    }

    /**
     * Removes a client from the server
     *
     * @param id Client id
     */
    public void removeEntity(int id) {
        Integer index = null;
        for (int i = 0; i < entities.size; i++) {
            if (entities.get(i).id == id)
                index = i;
        }

        if (index != null) {
            entities.remove(index);
        } else {
            log.error("removeEntity", "Tried to remove an entity that did not exist with id: " + id);
        }
    }

    /**
     * Update position of a given client
     *
     * @param id Client id
     * @param x  Position in X
     * @param y  Position in Y
     */
    public void updatePosition(int id, float x, float y) {
        for (int i = 0; i < entities.size; i++) {
            Entity entity = entities.get(i);
            if (entity.id == id) {
                entity.position.x = x;
                entity.position.y = y;
                newPosition(id, x, y);
                break;
            }
        }
    }
}
