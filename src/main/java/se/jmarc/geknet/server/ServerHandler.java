package se.jmarc.geknet.server;

import se.jmarc.geknet.common.logging.Log;
import se.jmarc.geknet.utils.Vector2;

/**
 * @author Marc
 * @version 1.0
 */
public interface ServerHandler extends Log {

    /**
     * Creates a list of connected ids
     *
     * @return List of connected client ids
     */
    int[] getClientIds();

    /**
     * Send an object over the network using TCP to a client
     *
     * @param id     Client id
     * @param object Object to send
     */
    void sendTCP(int id, Object object);

    /**
     * Send an object over the network using TCP to all client
     *
     * @param object Object to send
     */
    void sendTCPAll(Object object);

    /**
     * Send an object over the network using TCP to all except one client
     *
     * @param id     Client id
     * @param object Object to send
     */
    void sendTCPExcept(int id, Object object);

    /**
     * Send an object over the network using UDP to a client
     *
     * @param id     Client id
     * @param object Object to send
     */
    void sendUDP(int id, Object object);

    /**
     * Send an object over the network using UDP to all client
     *
     * @param object Object to send
     */
    void sendUDPAll(Object object);

    /**
     * Send an object over the network using UDP to all except one client
     *
     * @param id     Client id
     * @param object Object to send
     */
    void sendUDPExcept(int id, Object object);

    /**
     * Add a client to the server
     *
     * @param id Client id
     */
    void addEntity(int id);

    /**
     * Checks if client exists on the server
     *
     * @param id Client id
     * @return If client exists on the server
     */
    boolean hasEntity(int id);

    /**
     * Removes a client from the server
     *
     * @param id Client id
     */
    void removeEntity(int id);

    /**
     * Update position of a given client
     *
     * @param id Client id
     * @param x  Position in X
     * @param y  Position in Y
     */
    void updatePosition(int id, float x, float y);

    /**
     * Called when a client connects to the server
     *
     * @param id Client id
     */
    void clientConnected(int id);

    /**
     * Called when a client disconnects from the server
     *
     * @param id Client id
     */
    void clientDisconnected(int id);

    /**
     * Update the values of the provided Vector2 with the position of the entity that belongs to this client. This is
     * used in order to prevent object allocation.<br>
     * position.x = ...;<br>
     * position.y = ...;
     *
     * @param id       Id of client to get position for
     * @param position Object used to get the position and velocity of the entity
     */
    void getPosition(int id, Vector2 position);

    /**
     * New position received for a client
     *
     * @param id Id of client to get position for
     * @param x  X coordinate of client
     * @param y  Y coordinate of client
     */
    void newPosition(int id, float x, float y);

}
