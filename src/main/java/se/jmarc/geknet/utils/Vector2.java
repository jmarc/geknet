package se.jmarc.geknet.utils;

/**
 * Vector with 2 values, convenient way of representing position
 *
 * @author Marc Jamot
 */
public class Vector2 {

    /** Coordinate for X */
    public float x;
    /** Coordinate for Y */
    public float y;

    /**
     * Initializes with position (0,0)
     */
    public Vector2() {
    }

    /**
     * Initializes with given position
     *
     * @param x Coordinate for X
     * @param y Coordinate for Y
     */
    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Sets the position
     *
     * @param x Coordinate for X
     * @param y Coordinate for Y
     */
    public void set(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Sets the position to (0,0)
     */
    public void zero() {
        x = 0;
        y = 0;
    }

    /**
     * Sets the position
     *
     * @param vector2 Position to use
     */
    public void set(Vector2 vector2) {
        x = vector2.x;
        y = vector2.y;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector2 vector2 = (Vector2) o;

        if (Float.compare(vector2.x, x) != 0) return false;
        if (Float.compare(vector2.y, y) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        return result;
    }
}
