package se.jmarc.geknet.utils;

import com.esotericsoftware.kryo.Kryo;
import se.jmarc.geknet.common.Configuration;
import se.jmarc.geknet.common.logging.Log;
import se.jmarc.geknet.common.objects.*;
import se.jmarc.geknet.common.objects.Shutdown;
import se.jmarc.geknet.common.serializers.EntityArraySerializer;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Base components that are shared between the client and server
 *
 * @author Marc Jamot
 */
public abstract class GekBase {

    /** Holds all the Runnable that are to be run on the main thread */
    protected final BlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();

    /** Host to connect to */
    protected final String host;
    /** Port for TCP */
    protected final int tcpPort;
    /** Port for UDP */
    protected final Integer udpPort;
    /** If UDP connection is available */
    protected final boolean UDPConnection;
    /** Time between pulling received messages in seconds */
    protected final float runnableDelay;
    /** Time between broadcasting position */
    protected final float broadcastDelay;
    /** How logging should be made, null for none */
    protected final Log log;

    /** Kryo used to register new classes */
    private final Kryo kryo;

    /** Keeps track since last messages were pulled */
    private float currentRunnableDelay;
    /** Keeps track since last broadcast */
    private float currentBroadcastDelay;

    /**
     * Reads the configuration and registers custom classes
     *
     * @param configuration Configuration to be used.<br><b>IMPORTANT</b>: must be the same for both the client and server!
     */
    public GekBase(Configuration configuration) {
        this.host = configuration.host;
        this.tcpPort = configuration.tcpPort;
        this.udpPort = configuration.udpPort;
        this.UDPConnection = udpPort != null;
        this.runnableDelay = configuration.runnableDelay;
        this.broadcastDelay = configuration.broadcastDelay;
        this.log = configuration.log;

        this.kryo = initialize();
        kryo.register(ClientConnected.class);
        kryo.register(ClientDisconnected.class);
        kryo.register(ClientList.class);
        kryo.register(EntityArray.class, new EntityArraySerializer(configuration.maxPlayers));
        kryo.register(int[].class);
        kryo.register(ServerFull.class);
        kryo.register(Shutdown.class);
        kryo.register(Vector2.class);
    }

    /**
     * Registers a custom object to be sent between the client and server
     *
     * @param customObject Custom object<br><b>IMPORTANT</b>: order must be the same as for both the client and server!
     */
    protected void registerKryo(CustomObject customObject) {
        if (customObject.tSerializer == null) {
            kryo.register(customObject.tClass);
        } else {
            kryo.register(customObject.tClass, customObject.tSerializer);
        }
    }

    /**
     * Initialize the EndPoint
     *
     * @return The Kryo given by the EndPoint
     */
    public abstract Kryo initialize();

    /**
     * Open the connection
     *
     * @throws IOException If connection failed to start
     */
    public abstract void connect() throws IOException;

    /** Close the connection */
    public abstract void disconnect();

    /**
     * Should be called every game loop and will broadcast all required information<br>
     * In order to adjust how often messages are sent, check Configuration
     *
     * @param delta Time elapsed since last call in seconds
     */
    public void update(float delta) {
        // Handle received messages
        currentRunnableDelay += delta;
        while (currentRunnableDelay > runnableDelay) {
            currentRunnableDelay -= runnableDelay;

            for (Runnable runnable = queue.poll(); runnable != null; runnable = queue.poll()) {
                runnable.run();
            }
        }

        // Send position
        currentBroadcastDelay += delta;
        while (currentBroadcastDelay > broadcastDelay) {
            currentBroadcastDelay -= broadcastDelay;

            onBroadcast();
        }
    }

    /** Broadcast information */
    public abstract void onBroadcast();
}
