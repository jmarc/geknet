package se.jmarc.geknet.utils;

/**
 * Representation of a client
 *
 * @author Marc Jamot
 */
public class Entity {

    /** Position */
    public final Vector2 position = new Vector2();
    /** Client id */
    public int id;

    @Override
    public String toString() {
        return "Entity[" + id + "]" + position;
    }
}
