package se.jmarc.geknet.utils;

import com.esotericsoftware.kryo.Serializer;

/**
 * Convenient way of adding classes to the client
 *
 * @param <Connection> Handler to the client or server
 * @param <Handler>    Handler to connect to game situations
 * @param <Type>       Type of object to handle
 * @author Marc Jamot
 */
public abstract class CustomObject<Connection, Handler, Type> {

    /** Reference to class for registering with Kryo */
    public final Class<Type> tClass;
    /** Optional serializer */
    public Serializer<Type> tSerializer = null;

    /**
     * Sets required references
     *
     * @param tClass Reference to class for registering with Kryo
     */
    public CustomObject(Class<Type> tClass) {
        this.tClass = tClass;
    }

    /**
     * Sets required references
     *
     * @param tClass      Reference to class for registering with Kryo
     * @param tSerializer Serializer for the object when sending and receiving the data
     */
    public CustomObject(Class<Type> tClass, Serializer<Type> tSerializer) {
        this.tClass = tClass;
        this.tSerializer = tSerializer;
    }

    /**
     * When the object is received, this method will handle it<br>
     * You will need to cast the object to the correct type:<br>
     * (T) object;
     *
     * @param connection Handler to the client or server
     * @param handler    Handler to connect to game situations
     * @param object     Object received
     */
    public abstract void receive(Connection connection, Handler handler, Object object);
}