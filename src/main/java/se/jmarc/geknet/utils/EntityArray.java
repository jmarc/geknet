package se.jmarc.geknet.utils;

import java.util.Arrays;

/**
 * Efficient pre-allocated array for holding entities. When an item is removed, the last item is put in that position.
 *
 * @author Marc Jamot
 */
public class EntityArray {

    /** Size of the pre-allocated array */
    public final int capacity;
    /** Pre-allocated array that holds the data */
    private final Entity[] items;
    /** Current amount of active items */
    public int size;

    /**
     * Allocates the array with given size
     *
     * @param capacity Size to pre-allocate
     */
    public EntityArray(int capacity) {
        this.capacity = capacity;
        this.items = new Entity[capacity];
    }

    /**
     * Gets an element from the array
     *
     * @param index Index of the element
     * @return The found element
     */
    public Entity get(int index) {
        return items[index];
    }

    /**
     * Adds an element to the array
     *
     * @param item Element to add
     */
    public void add(Entity item) {
        if (size == capacity)
            throw new ArrayIndexOutOfBoundsException("Array is full, can't add item");

        items[size] = item;
        size++;
    }

    /**
     * Removes an element from the given position
     *
     * @param index Index of the element
     */
    public void remove(int index) {
        if (index < (size - 1)) {
            items[index] = items[size - 1];
            items[size - 1] = null;
        } else {
            items[index] = null;
        }
        size--;
    }

    @Override
    public String toString() {
        return "EntityArray: " + Arrays.toString(items);
    }
}
